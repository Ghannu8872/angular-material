import { Component, OnInit } from '@angular/core';

declare const $: any;
declare interface RouteInfo {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const ROUTES: RouteInfo[] = [
  { path: '/dashboard', title: 'Dashboard', icon: 'dashboard', class: '' },
  { path: '/users', title: 'Users', icon: 'person', class: '' },
  { path: '/accidental', title: 'Accidental Reporting', icon: 'description', class: '' },
  { path: '/manage_application', title: 'Manage Application', icon: 'create', class: '' },

];
declare interface RouteInfo1 {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const data_management: RouteInfo1[] = [
  { path: '/functional_activites', title: 'Functional Activities', icon: 'directions_run', class: '' },
  { path: '/status_update', title: 'Status Update', icon: 'dvr', class: '' },
];
declare interface RouteInfo2 {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const tracker_update: RouteInfo2[] = [
  { path: '/functional_tracker', title: 'Functional Tracker', icon: 'transfer_within_a_station', class: '' },
  { path: '/painsymptoms', title: 'Pain Symptoms', icon: 'local_pharmacy', class: '' },
];

declare interface RouteInfo3 {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const client_data: RouteInfo3[] = [
  { path: '/specials', title: 'Client Data - Specials', icon: 'trip_origin', class: '' },
  { path: '/referrals', title: 'Client Data - Referrals', icon: 'trip_origin', class: '' },
  { path: '/services', title: 'Client Data - Services', icon: 'trip_origin', class: '' },
  { path: '/employment', title: 'Client Data - Employement', icon: 'trip_origin', class: '' },
];

declare interface RouteInfo4 {
  path: string;
  title: string;
  icon: string;
  class: string;
}
export const extra: RouteInfo4[] = [
  { path: '/painsymptoms/view_pain_symptioms', title: 'View pain Symptoms', icon: '', class: '' },
  { path: '/functional_tracker/view_functional_status', title: 'View Functional Status', icon: '', class: '' },
  { path: '/specials/specials_view', title: 'Client Data Special View', icon: '', class: '' },
  { path: '/referrals/referral_view', title: 'Client Data Referrals View', icon: '', class: '' },
  { path: '/services/services_view', title: 'Client Data Services View', icon: '', class: '' },
  { path: '/manage_application/new_appointment', title: 'Add New Appointment', icon: '', class: '' },
  { path: '/accidental/report_accident', title: 'Report New Accident', icon: '', class: '' },

];

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {
  menuItems: any[];
  menuItems1: any[];
  menuItems2: any[];
  menuItems3: any[];
  show: boolean;

  constructor() { }

  ngOnInit() {
    this.menuItems = ROUTES.filter(menuItem => menuItem);
    this.menuItems1 = data_management.filter(menuItem1 => menuItem1);
    this.menuItems2 = tracker_update.filter(menuItem2 => menuItem2);
    this.menuItems3 = client_data.filter(menuItem3 => menuItem3);
  }
  isMobileMenu() {
    if ($(window).width() > 991) {
      return false;
    }
    return true;
  };
}
