import { LoginService } from './../../Services/login.service';
import { Component, OnInit } from '@angular/core';

import { Validators, FormGroup,FormControl, } from '@angular/forms';

import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {
  login: FormGroup = new FormGroup({
    
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
    
  });
  initializeFormGroup() {
    this.login.setValue({     
      username: '',
      password: '',      
    });
  }

  constructor(private router:Router) { }
  ngOnInit() {
  }
  
  onLogin(){   
    console.log(this.login.value);   
    this.router.navigate(['/dashboard']);    
}

}
