import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FunctionalActivitiesComponent } from './functional-activities.component';

describe('FunctionalActivitiesComponent', () => {
  let component: FunctionalActivitiesComponent;
  let fixture: ComponentFixture<FunctionalActivitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FunctionalActivitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FunctionalActivitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
