import { AddNewSubactivityComponent } from './../add-new-subactivity/add-new-subactivity.component';
import { AddNewActivityComponent } from './../add-new-activity/add-new-activity.component';

import { Component,OnInit, Inject } from '@angular/core';
import {MatDialog} from '@angular/material';

@Component({
  selector: 'app-functional-activities',
  templateUrl: './functional-activities.component.html',
  styleUrls: ['./functional-activities.component.scss']
})
export class FunctionalActivitiesComponent implements OnInit {
  ngOnInit(): void {
  }

  constructor(public dialog: MatDialog) { }
  openDialog(): void {
    const dialogRef = this.dialog.open(AddNewActivityComponent, {
      width: '500px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }
  openDialog1(): void {
    const dialogRef = this.dialog.open(AddNewSubactivityComponent, {
      width: '500px',
    });

    dialogRef.afterClosed().subscribe(result => {
    });
  }
}
