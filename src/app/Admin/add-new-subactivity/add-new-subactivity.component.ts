import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA} from '@angular/material';
export interface DialogData {
 
  }
@Component({
  selector: 'app-add-new-subactivity',
  templateUrl: './add-new-subactivity.component.html',
  styleUrls: ['./add-new-subactivity.component.scss']
})
export class AddNewSubactivityComponent implements OnInit {
  constructor(
    public dialogRef: MatDialogRef<AddNewSubactivityComponent>,
    @Inject(MAT_DIALOG_DATA) public data: DialogData) {}

  onNoClick(): void {
    this.dialogRef.close();
  }
  ngOnInit() {
  }

}
