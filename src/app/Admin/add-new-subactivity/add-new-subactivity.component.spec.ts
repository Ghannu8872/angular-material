import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddNewSubactivityComponent } from './add-new-subactivity.component';

describe('AddNewSubactivityComponent', () => {
  let component: AddNewSubactivityComponent;
  let fixture: ComponentFixture<AddNewSubactivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddNewSubactivityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddNewSubactivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
