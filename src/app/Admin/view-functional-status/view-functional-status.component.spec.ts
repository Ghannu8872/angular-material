import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewFunctionalStatusComponent } from './view-functional-status.component';

describe('ViewFunctionalStatusComponent', () => {
  let component: ViewFunctionalStatusComponent;
  let fixture: ComponentFixture<ViewFunctionalStatusComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewFunctionalStatusComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewFunctionalStatusComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
