import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PainSymptomsComponent } from './pain-symptoms.component';

describe('PainSymptomsComponent', () => {
  let component: PainSymptomsComponent;
  let fixture: ComponentFixture<PainSymptomsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PainSymptomsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PainSymptomsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
