import { Component, OnInit } from '@angular/core';
import { FormsModule, ReactiveFormsModule, FormBuilder,  } from '@angular/forms';
import { Validators, FormGroup,FormControl, FormArray} from '@angular/forms';
import { ToastrService } from 'ngx-toastr';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-appointment',
  templateUrl: './new-appointment.component.html',
  styleUrls: ['./new-appointment.component.scss']
})
export class NewAppointmentComponent implements OnInit {
  Linear = true;
  city: string;
  province: string;
  preferred_office: string;
  
  new_appointment;

  add_appointment: FormGroup = new FormGroup({
    $key: new FormControl(null),
    area_of_law: new FormControl('', Validators.required),
       
  }); 
  add_appointment1: FormGroup = new FormGroup({
 
    client_type: new FormControl('', Validators.required),
       
  }); 
  add_appointment2: FormGroup = new FormGroup({
    name: new FormControl('',[Validators.required, Validators.maxLength(60),  Validators.minLength(3)]),
    dob: new FormControl('', Validators.required),
    tel: new FormControl('',  [Validators.required, Validators.pattern(/(\(?[0-9]{3}\)?-?\s?[0-9]{3}-?[0-9]{4})/)   ]),
    email: new FormControl('', [ Validators.required,  Validators.email,]),
    city: new FormControl('', Validators.required),
    province: new FormControl('', Validators.required),
    office: new FormControl('', Validators.required),
    contect_method: new FormControl('', Validators.required),   
    describe: new FormControl('', Validators.required),     
  }); 

  constructor(private _formBuilder: FormBuilder, private toastr: ToastrService) {}

  
  ngOnInit() {
  }
  onSubmit(){  
    console.log(this.add_appointment.value);  
    console.log(this.add_appointment1.value);      
    console.log(this.add_appointment2.value);    
     
     }
}

