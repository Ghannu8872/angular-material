import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FunctionalTrackerComponent } from './functional-tracker.component';

describe('FunctionalTrackerComponent', () => {
  let component: FunctionalTrackerComponent;
  let fixture: ComponentFixture<FunctionalTrackerComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FunctionalTrackerComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FunctionalTrackerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
