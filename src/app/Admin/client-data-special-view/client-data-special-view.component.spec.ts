import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientDataSpecialViewComponent } from './client-data-special-view.component';

describe('ClientDataSpecialViewComponent', () => {
  let component: ClientDataSpecialViewComponent;
  let fixture: ComponentFixture<ClientDataSpecialViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientDataSpecialViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientDataSpecialViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
