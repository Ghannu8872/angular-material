import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ViewPainSymptomsComponent } from './view-pain-symptoms.component';

describe('ViewPainSymptomsComponent', () => {
  let component: ViewPainSymptomsComponent;
  let fixture: ComponentFixture<ViewPainSymptomsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ViewPainSymptomsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ViewPainSymptomsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
