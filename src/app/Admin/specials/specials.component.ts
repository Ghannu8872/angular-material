
import { Component, OnInit, ViewChild } from '@angular/core';
import {MatTableDataSource, MatSort, MatPaginator} from '@angular/material';

export interface pain_symtoms {

  
  name: string;
  email: string;
  ph_number: number;
 
}

const symtoms: pain_symtoms[] = [
  { name: 'Ghansham', email:'Ghansham@armyspy.com', ph_number: 6284069942,  },
  { name: 'Gautam', email:'Gautam@armyspy.com', ph_number: 5485624852, },
  { name: 'Soneet', email:'Soneet@armyspy.com', ph_number: 9658425618,},
  { name: 'Aman', email:'Aman@armyspy.com', ph_number: 8596472516,},
  { name: 'Krishh', email:'Krishh@armyspy.com', ph_number: 8549625482, },
  { name: 'Gurpreet', email:'Gurpreet@armyspy.com', ph_number: 458756155,},

];
@Component({
  selector: 'app-specials',
  templateUrl: './specials.component.html',
  styleUrls: ['./specials.component.scss']
})
export class SpecialsComponent implements OnInit {
  displayedColumns: string[] = [ 'name', 'email', 'ph_number', 'action'];
  dataSource; 
 
  @ViewChild( MatSort, {static: false}) sort: MatSort;
  @ViewChild( MatPaginator, {static: false} ) paginator: MatPaginator;
  
  getusers() {
        
         this.dataSource = new MatTableDataSource(symtoms) ;
        console.log(this.dataSource);
        
        
  }
  

  constructor() { }

  ngOnInit() {this.getusers();
    setTimeout(() =>this.dataSource.sort = this.sort);    
    setTimeout(()=>this.dataSource.paginator = this.paginator);  
  }

  applyFilter(FilterValue: string){
    this.dataSource.filter = FilterValue.trim().toLocaleLowerCase();    
   }

}