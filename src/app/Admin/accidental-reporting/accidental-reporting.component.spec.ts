import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AccidentalReportingComponent } from './accidental-reporting.component';

describe('AccidentalReportingComponent', () => {
  let component: AccidentalReportingComponent;
  let fixture: ComponentFixture<AccidentalReportingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AccidentalReportingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AccidentalReportingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
