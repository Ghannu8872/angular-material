import { Component, OnInit, ViewChild } from '@angular/core';
import {MatTableDataSource, MatSort, MatPaginator} from '@angular/material';

export interface acdident_reporting {  
  name: string;
  date_of_accident: string;
  location: string;
}

const accident: acdident_reporting[] = [
  {name: 'Ghansham', date_of_accident:'Ghansham@armyspy.com', location: 'Mohali' },
  {name: 'Gautam', date_of_accident:'Gautam@armyspy.com', location: 'Chandigarh' },
  {name: 'Soneet', date_of_accident:'Soneet@armyspy.com', location: 'Mohali' },
  {name: 'Aman', date_of_accident:'Aman@armyspy.com', location: 'Kharar' },
  {name: 'Soneet', date_of_accident:'Soneet@armyspy.com', location: 'Kurali' },
  {name: 'Ram', date_of_accident:'Ram@armyspy.com', location: 'Anandpur Sahib' },
  {name: 'Jatin', date_of_accident:'Jatin@armyspy.com', location: 'punchkula' },
  {name: 'Gurpreet', date_of_accident:'Gurpreet@armyspy.com', location: 'Chandigarh' },
  {name: 'Harish', date_of_accident:'Harish@armyspy.com', location: 'Mohali' },
  {name: 'Karish', date_of_accident:'Karish@armyspy.com', location: 'Kharar' },
  {name: 'Jeevan', date_of_accident:'Jeevan@armyspy.com', location: 'Mohali' },
  {name: 'Joy', date_of_accident:'Joy@armyspy.com', location: 'Ropar' },

  
];
@Component({
  selector: 'app-accidental-reporting',
  templateUrl: './accidental-reporting.component.html',
  styleUrls: ['./accidental-reporting.component.scss']
})
export class AccidentalReportingComponent implements OnInit {
  displayedColumns: string[] = [  'name', 'date_of_accident', 'location','action'];
  dataSource; 
 
  @ViewChild( MatSort, {static: false}) sort: MatSort;
  @ViewChild( MatPaginator, {static: false} ) paginator: MatPaginator;
  
  getappication() {
        
         this.dataSource = new MatTableDataSource(accident) ;
        console.log(this.dataSource);
        
        
  }

  constructor() { }

  ngOnInit() {
    this.getappication();
    setTimeout(() =>this.dataSource.sort = this.sort);    
        setTimeout(() =>this.dataSource.paginator = this.paginator); 
  }
  applyFilter(FilterValue: string){
    this.dataSource.filter = FilterValue.trim().toLocaleLowerCase();    
   }


}
