import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup,FormControl, } from '@angular/forms';

import { Router } from '@angular/router';
@Component({
  selector: 'app-forget-password',
  templateUrl: './forget-password.component.html',
  styleUrls: ['./forget-password.component.scss']
})
export class ForgetPasswordComponent implements OnInit {
  forget_password: FormGroup = new FormGroup({
    
    email: new FormControl('', Validators.required),
    
    
  });
  initializeFormGroup() {
    this.forget_password.setValue({     
      email: '',
       
    });
  }

  constructor(private router:Router) { }
  ngOnInit() {
  }
  
  onForget_password(){   
    console.log(this.forget_password.value);   
    this.router.navigate(['/login']);    
}

}
