import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientDataReferralsViewComponent } from './client-data-referrals-view.component';

describe('ClientDataReferralsViewComponent', () => {
  let component: ClientDataReferralsViewComponent;
  let fixture: ComponentFixture<ClientDataReferralsViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientDataReferralsViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientDataReferralsViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
