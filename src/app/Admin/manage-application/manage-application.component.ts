import { Component, OnInit,ViewChild } from '@angular/core';
import {MatTableDataSource, MatSort, MatPaginator} from '@angular/material';

export interface manage_application {

  date: string;  
  name: string;
  email: string;
  ph_number: number;
  pre_office: string;

}

const applications: manage_application[] = [
  { date: '15/07/2019', name: 'Ghansham', email:'Ghansham@armyspy.com', ph_number: 6284069942, pre_office: 'Mohali' },
  { date: '20/07/2019', name: 'Gautam', email:'Gautam@armyspy.com', ph_number: 5485624852, pre_office: 'Chandigarh' },
  { date: '15/05/2019', name: 'Soneet', email:'Soneet@armyspy.com', ph_number: 9658425618, pre_office: 'Delhi' },
  { date: '25/07/2019', name: 'Aman', email:'Aman@armyspy.com', ph_number: 8596472516, pre_office: 'Kharar' },
  { date: '15/12/2019', name: 'Krishh', email:'Krishh@armyspy.com', ph_number: 8549625482, pre_office: 'Kurali' },
  { date: '15/05/2019', name: 'Gurpreet', email:'Gurpreet@armyspy.com', ph_number: 458756155, pre_office: 'Ropar' },

];
@Component({
  selector: 'app-manage-application',
  templateUrl: './manage-application.component.html',
  styleUrls: ['./manage-application.component.scss']
})
export class ManageApplicationComponent implements OnInit {

  displayedColumns: string[] = [  'date', 'name', 'email', 'ph_number', 'pre_office','action'];
  dataSource; 
 
  @ViewChild( MatSort, {static: false}) sort: MatSort;
  @ViewChild( MatPaginator, {static: false} ) paginator: MatPaginator;
  
  getusers() {
        
         this.dataSource = new MatTableDataSource(applications) ;
        console.log(this.dataSource);
        
        
  }
  

  constructor() { }

  ngOnInit() {this.getusers();
    setTimeout(() =>this.dataSource.sort = this.sort);    
    setTimeout(() =>this.dataSource.paginator = this.paginator); 
  }

  applyFilter(FilterValue: string){
    this.dataSource.filter = FilterValue.trim().toLocaleLowerCase();    
   }
}
