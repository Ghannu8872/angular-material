import { Component, OnInit } from '@angular/core';
import { Validators, FormGroup, FormControl, } from '@angular/forms';

import { Router } from '@angular/router';
@Component({
  selector: 'app-report-accident',
  templateUrl: './report-accident.component.html',
  styleUrls: ['./report-accident.component.scss']
})
export class ReportAccidentComponent implements OnInit {
  report_accident: FormGroup = new FormGroup({

    date: new FormControl('', Validators.required),
    location: new FormControl('', Validators.required),
    owner_name: new FormControl('', Validators.required),
    driver_name: new FormControl('', Validators.required),
    driver_license_no: new FormControl('', Validators.required),
    license_plate_no: new FormControl('', Validators.required),
    policy_no: new FormControl('', Validators.required),
    vehicle_make: new FormControl('', Validators.required),
    vehicle_model: new FormControl('', Validators.required),
    insurance_company: new FormControl('', Validators.required),
    claim_no: new FormControl('', Validators.required),
    detail_of_accident: new FormControl('', Validators.required),
    

  });


  

  initializeFormGroup() {
    this.report_accident.setValue({
      date: '',
      location: '',
      owner_name: '',
      driver_name: '',
      driver_license_no: '',
      license_plate_no: '',
      policy_no: '',
      vehicle_make: '',
      vehicle_model: '',
      insurance_company: '',
      claim_no: '',
      detail_of_accident: '',


    });
  }

  constructor(private router: Router) { }
  step = 0;

  setStep(index: number) {
    this.step = index;
  }

  nextStep() {
    this.step++;
  }

  prevStep() {
    this.step--;
  }

  ngOnInit() {


  }
  onSubmit() {
    console.log(this.report_accident.value);
    // this.router.navigate(['/dashboard']);    
  }
  
}
