import { Component, OnInit, ViewChild} from '@angular/core';
import {MatTableDataSource, MatSort, MatPaginator} from '@angular/material';

export interface acdident_reporting {  
  name: string;
}
const status_update: acdident_reporting[] = [
  {name: 'Back Pain' },
  {name: 'Conentration Problems' },
  {name: 'Dizziness' },
  {name: 'Head Pain' },
  {name: 'Headaches' },
  {name: 'Jaw Pain' },
  {name: 'Left-arm Numbness' },
  {name: 'Nech Pain' },
  {name: 'Right-arm Numbess' },
  {name: 'Vision Problems' },
  
  
];
@Component({
  selector: 'app-status-update',
  templateUrl: './status-update.component.html',
  styleUrls: ['./status-update.component.scss']
})
export class StatusUpdateComponent implements OnInit {
  displayedColumns: string[] = [  'name','action'];
  dataSource; 
 
  @ViewChild( MatSort, {static: false}) sort: MatSort;
  @ViewChild( MatPaginator, {static: false} ) paginator: MatPaginator;
  
  getappication() {
        
         this.dataSource = new MatTableDataSource(status_update) ;
        console.log(this.dataSource);
        
        
  }

  constructor() { }

  ngOnInit() {
    this.getappication();
    setTimeout(() =>this.dataSource.sort = this.sort);    
    setTimeout(()=>this.dataSource.paginator = this.paginator);  
  }
}
