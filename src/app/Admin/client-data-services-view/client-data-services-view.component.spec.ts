import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClientDataServicesViewComponent } from './client-data-services-view.component';

describe('ClientDataServicesViewComponent', () => {
  let component: ClientDataServicesViewComponent;
  let fixture: ComponentFixture<ClientDataServicesViewComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClientDataServicesViewComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClientDataServicesViewComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
