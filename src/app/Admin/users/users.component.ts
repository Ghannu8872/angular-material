import { Component, OnInit,ViewChild } from '@angular/core';
import {MatTableDataSource, MatSort, MatPaginator} from '@angular/material';

export interface UserData {

  name: string;  
  email: string;
  status: string;
}

const Users: UserData[] = [
  { name:' Gautam', email:'Gautam@armyspy.com', status: 'Approve' },
  { name:' Ghansham', email:'Ghansham@armyspy.com', status: 'Approve' },
  { name:' Raj', email:'Raj@armyspy.com', status: 'Approve' },
  { name:' Jagga', email:'Jagga@armyspy.com', status: 'Reject' },
  { name:' Ram', email:'Ram@armyspy.com', status: 'Approve' },
  { name:' Jatin', email:'Jatin@armyspy.com', status: 'Reject' },
  { name:' Soneet', email:'Soneet@armyspy.com', status: 'Approve' },
  { name:' Gurpreet', email:'Gurpreet@armyspy.com', status: 'Approve' },
  { name:' Harpreet', email:'Harpreet@armyspy.com', status: 'Approve' },
   { name:' Krishh', email:'Krishh@armyspy.com', status: 'Approve' }

  
];



@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  
  displayedColumns: string[] = [ 'name', 'email', 'status','action'];
  dataSource; 

  @ViewChild( MatSort, {static: false}) sort: MatSort;
  @ViewChild( MatPaginator, {static: false} ) paginator: MatPaginator;
  
  getusers() {
        
         this.dataSource = new MatTableDataSource(Users) ;
        console.log(this.dataSource);
        
        setTimeout(() =>this.dataSource.sort = this.sort);       
        setTimeout(() =>this.dataSource.paginator = this.paginator); 
  }
  
  constructor() { }

  ngOnInit() {   
    this.getusers();
    
  }

  applyFilter(FilterValue: string){
    this.dataSource.filter = FilterValue.trim().toLocaleLowerCase();    
   }
}
