import { ForgetPasswordComponent } from './Admin/forget-password/forget-password.component';
import { NgModule } from '@angular/core';
import { CommonModule, } from '@angular/common';
import { BrowserModule  } from '@angular/platform-browser';
import { Routes, RouterModule } from '@angular/router';

import { AdminLayoutComponent } from './layouts/admin-layout/admin-layout.component';
import { LoginComponent } from './Admin/login/login.component';

const routes: Routes =[
  {path: '', component: LoginComponent }, 
  {path: 'login', component: LoginComponent }, 
  { path: 'forget_password', component: ForgetPasswordComponent },

  {path: '', component: AdminLayoutComponent,
    children: [
      {path: '', loadChildren: './layouts/admin-layout/admin-layout.module#AdminLayoutModule'}
               ]
  }
];

@NgModule({
  imports: [
    CommonModule,
    BrowserModule,
    RouterModule.forRoot(routes,{
       useHash: true
    })
  ],
  exports: [
  ],
})
export class AppRoutingModule { }
