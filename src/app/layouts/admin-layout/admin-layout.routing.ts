import { ManageApplicationComponent } from './../../Admin/manage-application/manage-application.component';
import { Routes } from '@angular/router';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import { UsersComponent } from './../../Admin/users/users.component';
import { AccidentalReportingComponent } from './../../Admin/accidental-reporting/accidental-reporting.component';

import { NewAppointmentComponent } from './../../Admin/new-appointment/new-appointment.component';
import { FunctionalActivitiesComponent } from './../../Admin/functional-activities/functional-activities.component';
import { StatusUpdateComponent } from './../../Admin/status-update/status-update.component';
import { EmploymentComponent } from './../../Admin/employment/employment.component';
import { ServicesComponent } from './../../Admin/services/services.component';
import { ReferralsComponent } from './../../Admin/referrals/referrals.component';
import { SpecialsComponent } from './../../Admin/specials/specials.component';
import { PainSymptomsComponent } from './../../Admin/pain-symptoms/pain-symptoms.component';
import { FunctionalTrackerComponent } from './../../Admin/functional-tracker/functional-tracker.component';
import { ViewFunctionalStatusComponent } from './../../Admin/view-functional-status/view-functional-status.component';
import { ViewPainSymptomsComponent } from './../../Admin/view-pain-symptoms/view-pain-symptoms.component';
import { ClientDataSpecialViewComponent } from './../../Admin/client-data-special-view/client-data-special-view.component';
import { ClientDataServicesViewComponent } from './../../Admin/client-data-services-view/client-data-services-view.component';
import { ClientDataReferralsViewComponent } from './../../Admin/client-data-referrals-view/client-data-referrals-view.component';
import { ReportAccidentComponent } from './../../Admin/report-accident/report-accident.component';
import { AddNewSubactivityComponent } from './../../Admin/add-new-subactivity/add-new-subactivity.component';
import { AddNewActivityComponent } from './../../Admin/add-new-activity/add-new-activity.component';


export const AdminLayoutRoutes: Routes = [

    // {
    //   path: '',
    //   children: [ {
    //     path: 'dashboard',
    //     component: DashboardComponent
    // }]}, {
    // path: '',
    // children: [ {
    //   path: 'userprofile',
    //   component: UserProfileComponent
    // }]
    // }, {
    //   path: '',
    //   children: [ {
    //     path: 'icons',
    //     component: IconsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'notifications',
    //         component: NotificationsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'maps',
    //         component: MapsComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'typography',
    //         component: TypographyComponent
    //     }]
    // }, {
    //     path: '',
    //     children: [ {
    //         path: 'upgrade',
    //         component: UpgradeComponent
    //     }]
    // }
    
    { path: 'dashboard', component: DashboardComponent },
    { path: 'users', component: UsersComponent },
    { path: 'accidental', component: AccidentalReportingComponent },
    { path: 'manage_application', component: ManageApplicationComponent },

    { path: 'manage_application/new_appointment', component: NewAppointmentComponent },
    { path: 'manage_application/new_appointment/:id', component: NewAppointmentComponent },
    { path: 'functional_activites', component: FunctionalActivitiesComponent },
    { path: 'status_update', component: StatusUpdateComponent },
    { path: 'employment', component: EmploymentComponent },
    { path: 'services', component: ServicesComponent },
    { path: 'referrals', component: ReferralsComponent },
    { path: 'specials', component: SpecialsComponent },
    { path: 'painsymptoms', component: PainSymptomsComponent },
    { path: 'functional_tracker', component: FunctionalTrackerComponent },
    { path: 'functional_tracker/view_functional_status', component: ViewFunctionalStatusComponent },
    { path: 'painsymptoms/view_pain_symptioms', component: ViewPainSymptomsComponent },
    { path: 'referrals/referral_view', component: ClientDataReferralsViewComponent },
    { path: 'specials/specials_view', component: ClientDataSpecialViewComponent },
    { path: 'services/services_view', component: ClientDataServicesViewComponent },    
    { path: 'accidental/report_accident', component: ReportAccidentComponent },
   
];
