import { AddNewActivityComponent } from './../../Admin/add-new-activity/add-new-activity.component';
import { ReportAccidentComponent } from './../../Admin/report-accident/report-accident.component';
import { ClientDataSpecialViewComponent } from './../../Admin/client-data-special-view/client-data-special-view.component';
import { ClientDataServicesViewComponent } from './../../Admin/client-data-services-view/client-data-services-view.component';
import { ClientDataReferralsViewComponent } from './../../Admin/client-data-referrals-view/client-data-referrals-view.component';
import { ViewFunctionalStatusComponent } from './../../Admin/view-functional-status/view-functional-status.component';
import { ViewPainSymptomsComponent } from './../../Admin/view-pain-symptoms/view-pain-symptoms.component';
import { FunctionalTrackerComponent } from './../../Admin/functional-tracker/functional-tracker.component';
import { EmploymentComponent } from './../../Admin/employment/employment.component';
import { ServicesComponent } from './../../Admin/services/services.component';
import { ReferralsComponent } from './../../Admin/referrals/referrals.component';
import { SpecialsComponent } from './../../Admin/specials/specials.component';
import { PainSymptomsComponent } from './../../Admin/pain-symptoms/pain-symptoms.component';
import { StatusUpdateComponent } from './../../Admin/status-update/status-update.component';
import { FunctionalActivitiesComponent } from './../../Admin/functional-activities/functional-activities.component';
import { NewAppointmentComponent } from './../../Admin/new-appointment/new-appointment.component';

import { ManageApplicationComponent } from './../../Admin/manage-application/manage-application.component';
import { AccidentalReportingComponent } from './../../Admin/accidental-reporting/accidental-reporting.component';
import { UsersComponent } from './../../Admin/users/users.component';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { AdminLayoutRoutes } from './admin-layout.routing';
import { DashboardComponent } from '../../dashboard/dashboard.component';
import {MatFormFieldModule, } from '@angular/material/form-field';
import {A11yModule} from '@angular/cdk/a11y';
import {DragDropModule} from '@angular/cdk/drag-drop';
import {PortalModule} from '@angular/cdk/portal';
import {ScrollingModule} from '@angular/cdk/scrolling';
import {CdkStepperModule} from '@angular/cdk/stepper';
import {CdkTableModule} from '@angular/cdk/table';
import {CdkTreeModule} from '@angular/cdk/tree';
import {MatAutocompleteModule} from '@angular/material/autocomplete';
import {MatBadgeModule} from '@angular/material/badge';
import {MatBottomSheetModule} from '@angular/material/bottom-sheet';
import {MatButtonModule} from '@angular/material/button';
import {MatButtonToggleModule} from '@angular/material/button-toggle';
import {MatCardModule} from '@angular/material/card';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatChipsModule} from '@angular/material/chips';
import {MatStepperModule} from '@angular/material/stepper';
import {MatDatepickerModule} from '@angular/material/datepicker';
import {MatDialogModule} from '@angular/material/dialog';
import {MatDividerModule} from '@angular/material/divider';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatGridListModule} from '@angular/material/grid-list';
import {MatIconModule} from '@angular/material/icon';
import {MatInputModule} from '@angular/material/input';
import {MatListModule} from '@angular/material/list';
import {MatMenuModule} from '@angular/material/menu';
import {MatNativeDateModule, MatRippleModule} from '@angular/material/core';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatProgressBarModule} from '@angular/material/progress-bar';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatRadioModule} from '@angular/material/radio';
import {MatSelectModule} from '@angular/material/select';
import {MatSidenavModule} from '@angular/material/sidenav';
import {MatSliderModule} from '@angular/material/slider';
import {MatSlideToggleModule} from '@angular/material/slide-toggle';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatSortModule} from '@angular/material/sort';
import {MatTableModule} from '@angular/material/table';
import {MatTabsModule} from '@angular/material/tabs';
import {MatToolbarModule} from '@angular/material/toolbar';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatTreeModule} from '@angular/material/tree';
import { HttpClientModule} from '@angular/common/http';
import { AddNewSubactivityComponent } from 'app/Admin/add-new-subactivity/add-new-subactivity.component';



@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(AdminLayoutRoutes),
    FormsModule,
    ReactiveFormsModule,
    MatButtonModule,
    MatRippleModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatTooltipModule,
    MatSliderModule,   
    MatButtonModule,
     
    MatInputModule,
    A11yModule,
    HttpClientModule,
    CdkStepperModule,
    CdkTableModule,
    CdkTreeModule,
    DragDropModule,
    MatAutocompleteModule,
    MatBadgeModule,
    MatBottomSheetModule,
    MatButtonModule,
    MatButtonToggleModule,
    MatCardModule,
    MatCheckboxModule,
    MatChipsModule,
    MatStepperModule,
    MatDatepickerModule,
    MatDialogModule,
    MatDividerModule,
    MatExpansionModule,
    MatGridListModule,
    MatIconModule,
    MatInputModule,
    MatListModule,
    MatMenuModule,
    MatNativeDateModule,
    MatPaginatorModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatRadioModule,
    MatRippleModule,
    MatSelectModule,
    MatSidenavModule,
    MatSliderModule,
    MatSlideToggleModule,
    MatSnackBarModule,
    MatSortModule,
    MatTableModule,
    MatTabsModule,
    MatToolbarModule,
    MatTooltipModule,
    MatTreeModule,
    PortalModule,
    ScrollingModule,
    


    
  ],
  exports: [
    MatTableModule,
    MatPaginatorModule,
    MatSortModule
 ],
  declarations: [
    DashboardComponent,
   
    UsersComponent,
    AccidentalReportingComponent,
    ManageApplicationComponent,
    NewAppointmentComponent,
    FunctionalActivitiesComponent,
    StatusUpdateComponent,
    FunctionalActivitiesComponent,
    PainSymptomsComponent,
    SpecialsComponent,
    ReferralsComponent,
    ServicesComponent,
    EmploymentComponent,
    FunctionalTrackerComponent,
    ViewPainSymptomsComponent,
    ViewFunctionalStatusComponent,
    ClientDataReferralsViewComponent,
    ClientDataServicesViewComponent,
    ClientDataSpecialViewComponent,
    ReportAccidentComponent,
    AddNewActivityComponent,
    AddNewSubactivityComponent
  ],
  entryComponents: [AddNewActivityComponent, AddNewSubactivityComponent]
  
})

export class AdminLayoutModule {}
